# Gifnix
My idea is to make a simple mp4 recorder for Linux that works similarly to ShareX

### Dependencies
* GTK+3
* cairo

but who doesn't have this

### Bugs
* a lot, probably

### Notes
The program saves the video as ~/output.mp4 and you can pass an external program or script as an argument (simple upload script)

## Notes on the gfycat script

I have included a shell script that tries to upload a file to gfycat. My shell scripts are horrible so it's probably better to write
one yourself. If no arguments are given to the script, it assumes default path of /tmp/output.mp4

The script attempts to print the finished URL to stdout for piping.

Example use (with xclip):
```shell
sh gfycat.sh ~/videos/video.mp4 | xclip -selection c
```
Note that the video path has to be absolute.

Upload process and potential error messages can be piped from stderr as well.
Here, no video path is given so the script assumes a path of ~/output.mp4.
```shell
sh gfycat.sh 2> errors.txt | xclip -selection c
```

Do note that a gfycat ID and secret key is required. You can get them from here: https://developers.gfycat.com/api