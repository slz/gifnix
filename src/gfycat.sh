# If no arguments
ARG=$1
FILE=$ARG
if [ -z "$ARG" ]; then
    echo "No file given. Assuming default path of /tmp/output.mp4" >&2
    FILE=/tmp/output.mp4
fi
if [ ! -f "$FILE" ]; then
    echo  "$FILE does not exist. Exiting"
    exit 1
fi

# Auth
URL=https://api.gfycat.com/v1/oauth/token
# Get gfycat info
URL2=https://api.gfycat.com/v1/gfycats
# Upload
URL3=https://filedrop.gfycat.com
# Check status
URL4=https://api.gfycat.com/v1/gfycats/fetch/status  #/GFYID <-

# Change these #
# https://developers.gfycat.com/api/ #
ID=
SECRET=
# Example:
#ID=j2a_52i
#SECRET=5j6a06j2lv-aJfgj20586jfmz6033_51jf8z02966n1lfjx6j82190g10agagjc
# # #

TOKEN=$(curl -d '{"client_id":"'$ID'", "client_secret":"'$SECRET'", "grant_type": "client_credentials"}' $URL | python -c "import sys, json; print(json.load(sys.stdin)['access_token'])")

AUTH="{\"Authorization\":\"Bearer $TOKEN\"}"

GFYID=$(curl $URL2 -H "Content-Type: application/json" -X POST -d "$AUTH" | python -c "import sys, json; print(json.load(sys.stdin)['gfyname'])")

# File needs to be named appropriatly
cp $FILE /tmp/$GFYID

# Upload
KEK=$(curl -v -i $URL3 --upload-file /tmp/$GFYID)

TASK=encoding
TMP=0
let count=0
while [ "$TASK" = "encoding" ]; do
    if [ "$count" -gt 10 ]; then
        echo "Maximum amount of attempts reached. Exiting"
        exit 1
    fi
    TMP=$(curl -X GET $URL4/$GFYID)
    TASK=$(echo $TMP | python -c "import sys, json; print(json.load(sys.stdin)['task'])")

    let count++
    echo "Attempt $count..." >&2

    # Check back in 3 seconds
    sleep 3
done


if [ "$TASK" = "error" ]; then
    echo "gfycat error for some reason"
    exit 1
fi
if [ "$TASK" = "NotFoundo" ]; then
    echo "couldn't find gfycat (probably not uploaded?)"
    exit 1
fi

# Parse name
FINAL=$(echo $TMP | python -c "import sys, json; print(json.load(sys.stdin)['gfyname'])")


# This is for direct .webm link
#FINAL=$(echo $TMP | python -c "import sys, json; print(json.load(sys.stdin)['webmUrl'])")

CLIP=http://gfycat.com/$FINAL
echo $CLIP                      # set clipboard
