#include <cairo/cairo.h>
#include <gtk/gtk.h>
#include <argp.h>
#include <stdlib.h>

#define BACKGROUND_TRANSPARENCY 0.5
#define COMMAND "/bin/sh -c '"
#define FRAMERATE 60

// don't want to record when testing stuff
#define NO_RECORD 0
#define NO_SEND 0

/*
========================================

                            ARGUMENTS

========================================
*/

/* https://www.gnu.org/software/libc/manual/html_node/Argp-Example-3.html#Argp-Example-3 */
const char *argp_program_version = "gifnix 0.1";
const char *argp_program_bug_address = "https://gitlab.com/slz/gifnix";
static char doc[] = "Gifnix - a simple screen recorder using ffmpeg";

static struct argp_option options[] = {
        {"program", 'p',  "PROGRAM",      0,  "Program to start when done recording" },
        { 0 }
};

/* Used by main to communicate with parse_opt. */
struct arguments
{
    char *programpath;
};
struct arguments arguments;

/* Parse a single option. */
static error_t parse_opt (int key, char *arg, struct argp_state *state)
{
    /* Get the input argument from argp_parse, which we
       know is a pointer to our arguments structure. */
    struct arguments *arguments = state->input;

    switch (key) {
        case 'p':
            arguments->programpath = arg;
            break;

        default:
            return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

/* Our argp parser. */
static struct argp argp = { options, parse_opt, 0, doc };

/*====================================*/



static GtkWidget *widget_main;
static GtkWidget *widget_dialog;

/* Stop ffmpeg from spawning twice */
static int ffmpeg_running = 0;

static cairo_rectangle_int_t resolution;

struct DragData {
    cairo_rectangle_int_t rect;     /**< Rectangle used for drawing */
    int x;                          /**< The current mouse y coordinate */
    int y;                          /**< The current mouse x coordinate */
    int x_start;                    /**< The mouse x coordinate when it was first clicked */
    int y_start;                    /**< The mouse y coordinate when it was first clicked */
    int dragging;                   /**< Mouse is currently down */
};

/* Prototypes */
static void ShowDialog(GtkWidget *widget, struct DragData *data);
static void Close();

/*
 * Sends output file to external program
 */
static void SendFile()
{
#if NO_SEND
    return;
#endif
    GError *err = NULL;

    if (!g_spawn_command_line_async(arguments.programpath, &err)) {
        fprintf (stderr, "Error: %s\n", err->message);
        g_error_free (err);
    }
}

static void SpawnFfmpeg(struct DragData *data)
{
#if NO_RECORD
    return;
#endif
    GError *err = NULL;
    /* ffmpeg command */
    char ffcall[256];

    //TODO: more ffmpeg arguments
    sprintf(ffcall, COMMAND "ffmpeg -f x11grab -s %ux%u -r %u -i :0.0+%u,%u ~/output.mp4'",
            data->rect.width, data->rect.height, FRAMERATE, data->x_start + resolution.x, data->y_start + resolution.y);

    /* Delete old file if it exists */
    if (!g_spawn_command_line_sync(COMMAND "rm -f ~/output.mp4'", NULL, NULL, NULL, &err)) {
        fprintf (stderr, "Error: %s\n", err->message);
    }
    err = NULL;

    /* Spawn ffmpeg process */
    if (!g_spawn_command_line_async(ffcall, &err)) {
        fprintf (stderr, "Error: %s\n", err->message);
        g_error_free (err);
        Close();
    }
    ffmpeg_running = 1;
    printf("%s\n", "Successfully spawned ffmpeg");
    printf("ffmpeg call: %s\n", ffcall);
}

static void Killffmpeg()
{
#if NO_RECORD
    return;
#endif
    GError *err = NULL;
    if (!g_spawn_command_line_sync (COMMAND "pkill -f ffmpeg'", NULL, NULL, NULL, &err)) {
        fprintf (stderr, "Error: %s\n", err->message);
        g_error_free (err);
    }
    ffmpeg_running = 0;
    printf("Successfully killed ffmpeg");
}

/* https://stackoverflow.com/questions/36994927/gtk3-window-transparent/37090303#37090303 */
static void InitDim(GtkWidget *win)
{
    GdkScreen *screen;
    GdkVisual *visual;

    gtk_widget_set_app_paintable(win, TRUE);
    screen = gdk_screen_get_default();
    visual = gdk_screen_get_rgba_visual(screen);

    if (visual != NULL && gdk_screen_is_composited(screen)) {
        gtk_widget_set_visual(win, visual);
    }
}

/*
========================================

                            EVENTS

========================================
*/

static int event_onMousePress(GtkWidget *widget, GdkEventButton *event, struct DragData *data)
{
    if (ffmpeg_running)
        return 0;
    if (event->button == GDK_BUTTON_PRIMARY) {
        const int x = (int) event->x;
        const int y = (int) event->y;

        data->rect.x = x;
        data->rect.y = y;
        data->x_start = x;
        data->y_start = y;
        data->dragging = 1;
        return 1;
    }
    return 0;
}

static int event_onMouseRelease(GtkWidget *widget, GdkEventButton *event, struct DragData *data)
{
    if (ffmpeg_running)
        return 0;
    if (event->button == GDK_BUTTON_PRIMARY) {
        data->dragging = 0;

        SpawnFfmpeg(data);

        ShowDialog(widget, data);

        // minimize
        gdk_window_iconify(gtk_widget_get_window(widget));
        return 1;
    }
    return 0;
}

static int event_onMouseMove(GtkWidget *widget, GdkEventMotion *event, struct DragData *data)
{
    if (data->dragging) {
        /* Store old data before making changes */
        cairo_rectangle_int_t oldrect = data->rect;
        cairo_region_t *region;

        const int x = (int) event->x;
        const int y = (int) event->y;

        data->x = x;
        data->y = y;

        int diffx = data->x - data->x_start;
        int diffy = data->y - data->y_start;

        if (diffx < 0)
            data->rect.x = x;
        if (diffy < 0)
            data->rect.y = y;

        /* Fix draw rectangle */
        data->rect.width = ABS(diffx) + 1;
        data->rect.height = ABS(diffy) + 1;

        /* Update draw area */
        region = cairo_region_create_rectangle(&oldrect);
        cairo_region_union_rectangle(region, &data->rect);
        gdk_window_invalidate_region(gtk_widget_get_window(widget), region, TRUE);
        cairo_region_destroy(region);

        return 1;
    }
    return 0;
}

static int event_onKeyPress(GtkWidget *widget, GdkEventKey *event, struct DragData *data)
{
    if (event->keyval == GDK_KEY_Escape) {
        Close();
        return 1;
    }

    return 0;
}

static int event_draw(GtkWidget *widget, cairo_t *cr, struct DragData *data)
{
    /* Draw transparent background */
    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, BACKGROUND_TRANSPARENCY);
    cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
    cairo_paint(cr);

    if (data->rect.width != 0 || data->rect.height != 0) {

        /* Draw mouse rectangle */
        cairo_set_source_rgba (cr, 1.0f, 1.0f, 1.0f, 0.0f);

        cairo_rectangle(cr, data->rect.x, data->rect.y, data->rect.width, data->rect.height);
        cairo_fill (cr);
    }

    return 0;
}

/*static void event_buttonClicked(GtkWidget *widget, struct DragData *data)
{

}*/

/*
 * Aborts further action. Does not send file to upload program
 */
static void event_dialogClosed()
{
    Killffmpeg();
    Close();
}

/*
 * Dialog that is drawn when ffmpeg is recording
 */
static void ShowDialog(GtkWidget *widget, struct DragData *data)
{
    GtkWidget *button;
    GtkWidget *button_box;

    widget_dialog = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(widget_dialog), "Gifnix");
    gtk_window_set_default_size(GTK_WINDOW(widget_dialog), 150, 50);
    gtk_window_set_position(GTK_WINDOW(widget_dialog), GTK_WIN_POS_NONE);

    gtk_window_move(GTK_WINDOW(widget_dialog), resolution.x + data->x - 150, resolution.y + data->y + 10);

    gtk_window_set_keep_above(GTK_WINDOW(widget_dialog), 1);

    button_box = gtk_button_box_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_container_add(GTK_CONTAINER(widget_dialog), button_box);

    button = gtk_button_new_with_label("Done");

  //  g_signal_connect(button, "clicked", G_CALLBACK(event_buttonClicked), widget_dialog);

    /* Close program when button is clicked */
    g_signal_connect(widget_dialog, "destroy", G_CALLBACK(event_dialogClosed), NULL);

    g_signal_connect_swapped(button, "clicked", G_CALLBACK(Close), NULL);

    gtk_container_add(GTK_CONTAINER(button_box), button);

    gtk_widget_show_all(widget_dialog);
}

/*
 * Disposes widgets and closes program
 */
static void Close()
{
    if (ffmpeg_running) {

        Killffmpeg();

        /* If program path is not empty */
        if (arguments.programpath != 0) {
            SendFile();
        }
    }

    /* Dispose widgets */
    GtkWidget *ptr;
    gtk_widget_destroyed(widget_dialog, &ptr);
    if (!ptr) {
        if(GTK_IS_WIDGET(ptr))
            gtk_widget_destroy(ptr);
    }
    gtk_widget_destroyed(widget_main, &ptr);
    if (!ptr) {
        if(GTK_IS_WIDGET(ptr))
            gtk_widget_destroy(ptr);
    }

    gtk_main_quit();
}

/*====================================*/
int main(int argc, char **argv)
{
    /* Default values. */
    arguments.programpath = 0;

    /* Parse our arguments; every option seen by parse_opt will
       be reflected in arguments. */
    argp_parse (&argp, argc, argv, 0, 0, &arguments);
    if (arguments.programpath == 0) {
        printf("%s\n", "No program path given. Recording only.");
    }

    GtkWidget *drawarea;
    gtk_init(&argc, &argv);
    widget_main = gtk_window_new(GTK_WINDOW_TOPLEVEL);

    InitDim(widget_main);
    drawarea = gtk_drawing_area_new();
    gtk_container_add(GTK_CONTAINER (widget_main), drawarea);

    /* DragData default values */
    struct DragData data;
    data.rect = (cairo_rectangle_int_t) {0, 0, 0, 0};
    data.x = 0;
    data.y = 0;
    data.x_start = 0;
    data.y_start = 0;
    data.dragging = 0;

    // For ffmpeg to write error logs to current directory
    g_setenv ("FFREPORT", "file=ffcom.log:level=32", 1);

    // draw event
    g_signal_connect(G_OBJECT(drawarea), "draw",
                     G_CALLBACK(event_draw), &data);
    // destroy event
    g_signal_connect(widget_main, "destroy",
                     G_CALLBACK(Close), NULL);
    // mouse click event
    g_signal_connect (widget_main, "button-press-event",
                      G_CALLBACK(event_onMousePress), &data);

    // mouse release event
    g_signal_connect (widget_main, "button-release-event",
                      G_CALLBACK(event_onMouseRelease), &data);

    // mouse move event
    g_signal_connect (widget_main, "motion-notify-event",
                      G_CALLBACK (event_onMouseMove), &data);

    // keyboard event
    g_signal_connect (widget_main, "key-press-event",
                      G_CALLBACK(event_onKeyPress), &data);



    gtk_window_set_position(GTK_WINDOW(widget_main), GTK_WIN_POS_CENTER);

    gtk_window_set_title(GTK_WINDOW(widget_main), "Gifnix");
    gtk_widget_show_all(widget_main);

    gtk_window_fullscreen((GtkWindow *) widget_main);


    /* Get resolution of current monitor */
    GdkDisplay *display;
    display = gdk_window_get_display(gtk_widget_get_window(widget_main));

    GdkMonitor *monitor;
    monitor = gdk_display_get_monitor_at_window(display, gtk_widget_get_window(widget_main));

    cairo_rectangle_int_t res;
    gdk_monitor_get_geometry(monitor, &res);
    resolution = res;

    gtk_main();

    return 0;
}